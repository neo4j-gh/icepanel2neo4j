const { readFileSync } = require("fs");

const content = JSON.parse(readFileSync("./icepanel.json", "utf-8"));

const neo4j = require("neo4j-driver");
const driver = neo4j.driver(
  "bolt://localhost:7687",
  neo4j.auth.basic("neo4j", "icepanel")
);

const run_query = (query) =>
  new Promise((resolve) => {
    (async () => {
      const session = driver.session();
      const result = await session.run(query);
      await session.close();
      resolve(result);
    })();
  });

const main = async () => {
  const { connections } = content;
  for (const connection of connections) {
    const { records } = await run_query("MATCH (a) RETURN a");
    for (const record of records) {
      console.log(record.get('a'));
    }
  }
};

main()
  .then(() => driver.close())
  .catch((err) => console.log(`error: ${err}`));
